FROM ghcr.io/jitesoft/alpine

COPY ./backup /usr/local/bin/backup

RUN apk add --no-cache openssl mysql-client rclone \
 && chmod +x /usr/local/bin/backup

CMD [ "backup" ]
